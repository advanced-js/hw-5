let urls = [
    "https://ajax.test-danit.com/api/json/posts",
    "https://ajax.test-danit.com/api/json/users"
    
]

class Request {
    getRes(arr){                                  //запрос на получение данных с сервера
      let requests = arr.map(url => fetch(url));
      let resp = Promise.all(requests)
        .then(responses => responses.map(i => i.json()))
        .catch(e => console.log(e))
        return resp;
    }

    deletePost(link, elem){                   // запрос на удаление данных с сервера
        (fetch(link, {
            method: 'DELETE',
        })) 
            .then(data => {
             if(data.ok){
                elem.remove()
                } else {
                    throw new Error("Что-то пошло не так!")
                }
        })
    }
}


class Cards {    
    createCards(){
        try {
            let req = new Request();
     req.getRes(urls)
     .then(function([posts, users]){                    
        posts.then((post) => {
           
            post.forEach(({userId, id, title, body}) => {
        let postId = id;
             users.then(user => {
                user.forEach(({name, id, email}) => {
                    //console.log(userId, title, body);
                    //console.log(name, id, email);
                    if(userId === id){
                        let username = document.createElement("span");
                        username.classList = "user";
                        let useremail = document.createElement("span");
                        useremail.classList = "mail"
                        let header = document.createElement("h3");
                        header.classList = "header"
                        let text = document.createElement("p");
                        text.classList = "text"
                        username.textContent = name;
                        useremail.textContent = email;
                        header.textContent = title;
                        text.textContent = body;
                        let div = document.createElement("div");
                        div.id = postId;

//удаление карточки
                        let btn = document.createElement("button");
                        btn.textContent = "x";
                        btn.classList = "btn"
                        btn.addEventListener("click", () => {

                            req.deletePost(`https://ajax.test-danit.com/api/json/posts/${postId}`, div)
                        })

                        div.append(username, useremail, btn, header, text);
                        div.classList = "card";
                        document.body.prepend(div);
                        return div;
                    }
                })
             })
            });
        })
     })
        } catch (e) {
            console.log(e);
        }
    } 
}

let card = new Cards();
card.createCards();










