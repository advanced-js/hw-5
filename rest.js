const urls = [
    "https://ajax.test-danit.com/api/json/posts",
    "https://ajax.test-danit.com/api/json/users"
    
]



class Request {
    getRes(arr){                                  //запрос на получение данных с сервера
      const requests = arr.map(url => fetch(url));
      const resp = Promise.all(requests)
        .then(responses => responses.map(i => i.json()))
        .catch(e => console.log(e))
        return resp;
    }

    deletePost(link){                   // запрос на удаление данных с сервера
       let response = (fetch(link, {
            method: 'DELETE',
        }))
        return response;
    }

    editPost(link, elem){             // запрос на редактирование поста
        return fetch(link, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(elem),
		})

     }
}

class Cards {

    createCards(data){
        const [posts, users] = data;
          posts.then((post) => {
            post.sort((a, b) => b.id - a.id);
            post.map(({userId, id, title, body}) => {
                let postId = id;
                users.then(user => {
                 user.map(({name, id, email}) => {
                    // console.log(userId, title, body);
                    // console.log(name, id, email);
                            if(userId === id){
                                let div = document.createElement("div");
                                div.classList = "card";
                                div.id = postId;

                                let username = document.createElement("span");
                                username.classList = "user";
                                let useremail = document.createElement("span");
                                useremail.classList = "mail"
                                let header = document.createElement("h3");
                                header.classList = "header"
                                let text = document.createElement("p");
                                text.classList = "text"
                                username.textContent = name;
                                useremail.textContent = email;
                                header.textContent = title;
                                text.textContent = body;
                                div.append(username, useremail, header, text);

                            
                                let btn = createElement(`btn-${postId}`, "btn", "img", "./close.png", div);
                                let edit = createElement(`edit-${postId}`, "image-edit", "img", "./mypen.png", div);

                                document.body.append(div); 
                                card.deleteCards(btn, postId, div);
                                card.editCard(edit, div, name, email, title, body, postId, username, useremail, header, text);

                                return div; 
                            }
                        })
                     })
                     
                });
               
            });
            return data;
    }


    deleteCards(btn, id, element){
                 
                 btn.addEventListener("click", () => {
                     req.deletePost(`https://ajax.test-danit.com/api/json/posts/${id}`)
                     .then(data => {
                        if(data.ok){
                           element.remove()
                           } else {
                               throw new Error("Что-то пошло не так!")
                           }
                   })
                    });
    }

    editCard(edit, div, name, email, title, body, postId, username, useremail, header, text){

        edit.addEventListener("click", () => {
         
            div.classList.toggle("editwindow");
            let editform = document.createElement("form");
            editform.classList.add("form-edit");
            let newName = document.createElement("input");
            newName.placeholder = name;
            let newEmail = document.createElement("input");
            newEmail.placeholder = email;
            let newTitle = document.createElement("input");
            newTitle.placeholder = title;
            let newBody = document.createElement("textarea");
            newBody.placeholder = body;
            const done = document.createElement("button");
            done.classList.add("done")
            done.textContent = "Yes"
            editform.append(newName, newEmail, newTitle, newBody, done);
            div.after(editform);
          
            
               
            done.addEventListener("click", (e)=> {
                e.preventDefault();
                if(newName.value === "" || newEmail.value === "" || newTitle.value === "" || newBody.value === ""){
                    alert("Fill in all the fields!")
                } else {
                    let editObj = {
                        body: newBody.value,
                        title: newTitle.value,
                        name: newName.value,
                        email: newEmail.value
                    }
                    req.editPost(`https://ajax.test-danit.com/api/json/posts/${postId}`, editObj)
                    .then(response => response.json())
                    .then((data) => {
                        let {body, email, title, name} = data;
                              username.textContent = name;
                              useremail.textContent = email;
                              header.textContent = title;
                              text.textContent = body;
                        editform.style.display = "none";
                        div.classList.remove("editwindow");
                    })
                    .catch(e => console.log(e))
                }
               })
        })

    }


    addPost(postId){
const addPost = document.createElement("button");
addPost.classList.add("add-post");
addPost.textContent = "Add Post";
document.body.prepend(addPost);

addPost.addEventListener("click", ()=> {
    modal.openModal();
    formpost.addPost(postId);
});
    }

}

let card = new Cards();
let req = new Request();


req.getRes(urls)
.then(data => card.createCards(data))
.then(data => card.addPost(data))


function createElement(id, classes, tag, path, parent){   // для создания кнопок удалить и редактировать
let element = document.createElement(tag);
element.classList = classes;
element.id = id;
element.src = path;
element.setAttribute("width", "20");
element.setAttribute("height", "20");
parent.append(element);
return element;
 }


 //модальное окно для добавления поста
class Modal {
    constructor(id, classes, text){
        this.id = id;
        this.classes = classes;
        this.text = text;
    }

    render(content = ""){
        this.window = document.createElement("div");
        this.window.id = this.id;
        this.window.classList.add(...this.classes);

        const modalContent = document.createElement("div");
        modalContent.classList.add("modal-content");
        const span = document.createElement("span");
        span.classList.add("close");
        span.innerHTML = "&times;";
        modalContent.append(span, content);
        this.window.append(modalContent);

        span.addEventListener("click", this.closeModal.bind(this));

        return this.window;
    }

    openModal(){
        this.window.classList.add("active");
    }
    closeModal(){
        this.window.classList.remove("active");
    }
}


class formPost extends Modal{
    constructor(id, classes, text) {
        super(id, classes, text);
    }



    createformPost(){
        const form = document.createElement("form");
        form.action = "";
        form.id = "post-form";

        const inputTitle = document.createElement("input");
        inputTitle.type = "text";
        inputTitle.name = "title";
        inputTitle.placeholder = "Enter post title...";
        inputTitle.id = "title-post";
        inputTitle.required = true;


        const inputText = document.createElement("textarea");
        inputText.name = "text";
        inputText.placeholder = "Enter your text...";
        inputText.required = true;
        inputText.id = "content-post";

        const submit = document.createElement("button");
        submit.type = "submit";
        submit.id = "submit";
        submit.textContent = "Submit";

        form.append(inputTitle, inputText, submit);
        return form;
    }

    addPost(postId){
       let submit = document.getElementById("submit");
       let inputText = document.getElementById("content-post");
       let inputTitle = document.getElementById("title-post");

       
    //             // добавление поста на сервер.  
                submit.addEventListener("click", (e) => {
                    e.preventDefault();
                    modal.closeModal();
                    const request = fetch("https://ajax.test-danit.com/api/json/posts", {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json;charset=utf-8'
                          },
                          body: JSON.stringify({
                            userId: 1,
                            body:  inputText.value,
                            title: inputTitle.value
                          }) 
                    });
        
                    request.then(response => response.json())
                    
                    .then(({id, body, title}) => {
                        let postid = id;
                        let newPost = document.createElement("div");
                        newPost.classList.add("card");
        
                                let username = document.createElement("span");
                                username.classList = "user";
                                let useremail = document.createElement("span");
                                useremail.classList = "mail"
                                let header = document.createElement("h3");
                                header.classList = "header"
                                let text = document.createElement("p");
                                text.classList = "text"
                                username.textContent = "Leanne Graham";
                                useremail.textContent = "Sincere@april.biz";
                                header.textContent = title;
                                text.textContent = body;
                                newPost.id = postid;


                                let btn = createElement(`btn-${postId}`, "btn", "img", "./close.png", newPost);
                                let edit = createElement(`edit-${postId}`, "image-edit", "img", "./mypen.png", newPost); 

                                edit.addEventListener("click", () => {

                                    newPost.classList.toggle("editwindow");
                                    let editform = document.createElement("form");
                                    editform.classList.add("form-edit");
                                    let newName = document.createElement("input");
                                    newName.placeholder = "your Name";
                                    let newEmail = document.createElement("input");
                                    newEmail.placeholder = "your email";
                                    let newTitle = document.createElement("input");
                                    newTitle.placeholder = title;
                                    let newBody = document.createElement("textarea");
                                    newBody.placeholder = body;
                                    const done = document.createElement("button");
                                     done.textContent = "Yes"
                                     editform.append(newName, newEmail, newTitle, newBody, done);
                                     newPost.after(editform);


                                    done.addEventListener("click", (e) => {
                                        console.log("исправляем здесь");
                                        e.preventDefault();
                                if(newName.value === "" || newEmail.value === "" || newTitle.value === "" || newBody.value === ""){
                                    alert("Fill in all the fields");
                                } else { 
                                    username.textContent = newName.value;
                                    username.textContent = newEmail.value;
                                    header.textContent = newTitle.value;
                                    text.textContent = newBody.value;
                                    editform.style.display = "none";
                                    newPost.classList.remove("editwindow")

                                }
                                      })

                                })

                                newPost.append(username, useremail, header, text, btn, edit);
                                let root = document.getElementById("root");
                                root.after(newPost);


           
                        btn.addEventListener("click", () => {
                            newPost.remove();
                        })

                                return newPost;
                    })
                    .catch(e => console.log(e));
                    inputText.value = "";
                    inputTitle.value = "";
                })
     }
}

const modal = new Modal("window", ["modal"], "Add post");
const formpost = new formPost();
const editModal = new Modal("window-edit", ["modal"], "Add post"); // модалка для редактирования поста 

const root = document.getElementById("root");
root.append(modal.render(formpost.createformPost()), editModal.render()); // добавление модальных окон




